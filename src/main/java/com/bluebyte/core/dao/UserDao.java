package com.bluebyte.core.dao;

import java.util.List;

import com.bluebyte.core.entity.User;

/**
 * 
 * @author ignashev
 *
 */

public interface UserDao {

	public User getUserByUsername(String username);
	
	public List<User> getAllUsers();
}
