package com.bluebyte.core.service;

import java.util.List;

import com.bluebyte.core.entity.User;


/**
 * 
 * @author ignashev
 *
 */

public interface UserService {

	public User getUserByUsername(String username);
	public List<User> getAllUsers();
}
